import Menu from './Menu'
import { useState } from 'react'
import data from './data'
import Title from './Title'
import Categories from './Categories'
const App = () => {
  const allCategories = ['all', ...new Set(data.map((item) => item.category))]
  const [dataItems, setDataItems] = useState(data)
  const [categoryItems, setCategoryItems] = useState(allCategories)
  const filterItems = (categoryName) => {
    if (categoryName === 'all') {
      setDataItems(data)
      return
    }
    const newFilter = data.filter((item) => item.category === categoryName)
    setDataItems(newFilter)
  }
  return (
    <main>
      <section className="menu">
        <Title text="our menu" />
        <Categories categoryItems={categoryItems} filterItems={filterItems} />
        <Menu dataItems={dataItems} />
      </section>
    </main>
  )
}
export default App
