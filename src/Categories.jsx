const Categories = ({ categoryItems, filterItems }) => {
  return (
    <div className="btn-container">
      {categoryItems.map((categoryName) => {
        return (
          <button
            key={categoryName}
            type="button"
            className="btn"
            onClick={() => filterItems(categoryName)}>
            {categoryName}
          </button>
        )
      })}
    </div>
  )
}
export default Categories
