import MenuItem from './MenuItem'
const Menu = ({ dataItems }) => {
  return (
    <div className="section-center">
      {dataItems.map((item) => {
        return <MenuItem key={item.id} {...item} />
      })}
    </div>
  )
}
export default Menu
